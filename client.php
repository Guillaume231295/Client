<?php



    class Client extends Module {

       

    //On initialise les données du module

    public function __construct(){

        $this->name = 'client'; 

        $this->tab = 'front_office_features';

        $this->version = '1.0.0';

        $this->author = 'Guillaume Module Client DV';

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => '1.6.1.18');

        $this->bootstrap = true;

        $this->displayName = $this->l('Client');

        $this->description = $this->l('The module of the client');

        $this->confirmUninstall = $this->l('Really ? Are you sure ? ');

        parent::__construct();

    }



    // On créer une fonction pour installer le module

    public function install(){

        if(!parent::install()

        ){

            return false;

        }

        return true;

    }



    // On créer une fonction pour la desinstallation du module

    public function uninstall(){

        if(!parent::uninstall() 

        ){

            return false;

        }

        return true;

    }


    // Configuration du module

    public function getContent(){

        return $this->postProcess().$this->renderForm();

    }



    public function postProcess(){

        //ON UPDATE LES VALEURS quand on clique sur le bouton submit

        if (Tools::isSubmit('submitConfModule')){

            Configuration::updateValue('CLIENT_TOKKEN', Tools::getValue('CLIENT_TOKKEN'));

            //Configuration::updateValue('reference', Tools::getValue('reference'));

            Configuration::updateValue('supplier_reference', Tools::getValue('supplier_reference'));

            Configuration::updateValue('location', Tools::getValue('location'));

            Configuration::updateValue('width', Tools::getValue('width'));

            Configuration::updateValue('height', Tools::getValue('height'));

            Configuration::updateValue('depth', Tools::getValue('depth'));

            Configuration::updateValue('weight', Tools::getValue('weight'));

            Configuration::updateValue('quantity_discount', Tools::getValue('quantity_discount'));

            Configuration::updateValue('ean13', Tools::getValue('ean13'));

            Configuration::updateValue('on_sale', Tools::getValue('on_sale'));

            Configuration::updateValue('online_only', Tools::getValue('online_only'));

            Configuration::updateValue('ecotax', Tools::getValue('ecotax'));

            Configuration::updateValue('minimal_quantity', Tools::getValue('minimal_quantity'));

            Configuration::updateValue('wholesale_price', Tools::getValue('wholesale_price'));

            Configuration::updateValue('customizable', Tools::getValue('customizable'));

            Configuration::updateValue('text_fields', Tools::getValue('text_fields'));

            Configuration::updateValue('uploadable_files', Tools::getValue('uploadable_files'));

            Configuration::updateValue('active', Tools::getValue('active'));

            Configuration::updateValue('available_for_order', Tools::getValue('available_for_order'));

            Configuration::updateValue('available_date', Tools::getValue('available_date'));

            Configuration::updateValue('show_price', Tools::getValue('show_price'));

            Configuration::updateValue('date_add', Tools::getValue('date_add'));

            Configuration::updateValue('date_upd', Tools::getValue('date_upd'));

            Configuration::updateValue('pack_stock_type', Tools::getValue('pack_stock_type'));

            Configuration::updateValue('meta_description', Tools::getValue('meta_description'));

            Configuration::updateValue('meta_keywords', Tools::getValue('meta_keywords'));

            Configuration::updateValue('meta_title', Tools::getValue('meta_title'));

            Configuration::updateValue('description', Tools::getValue('description'));

            Configuration::updateValue('description_short', Tools::getValue('description_short'));

            Configuration::updateValue('available_now', Tools::getValue('available_now'));

            Configuration::updateValue('available_later', Tools::getValue('available_later'));

            Configuration::updateValue('nom_prestashop', Tools::getValue('nom_prestashop'));

            Configuration::updateValue('adresse_centrale', Tools::getValue('adresse_centrale'));

                }

                return '';

            }



    //ON AFFICHE LE FORMULAIRES

    public function renderForm(){

        $Prestashop_dossier = Configuration::get('nom_prestashop');

        $tokken = Configuration::get('CLIENT_TOKKEN'); 

        //ON INITIALISE LES INFORMATIONS POUR RENDRE DYNAMIQUE LE MODULE CLIENTS

        $this->fields_form[0]['form'] = array(

            'legend' => array(

            'title' => $this->l('Information sur Prestashop Client et Centrale'),

            'icon' => 'icon-cogs'

            ),

            'input' => array(

                array(

                    'type' => 'text',

                    'label' => $this->l('Nom de dossier du client prestashop'),

                    'name' => 'nom_prestashop',

                ),

                array(

                    'type' => 'text',

                    'label' => $this->l('Nom de domaine de la centrale + nom de dossier'),

                    'name' => 'adresse_centrale',

                ),

            ),

            'submit' => array(

                'title' => $this->l('Save')

            )

        );

        $this->fields_form[1]['form'] = array(

            //ON INITIALISE LE TOKKEN DE SECURITE QUI PROTEGE LA CENTRALE

            'legend' => array(

                'title' => $this->l('Tokken de sécurité'),

                'icon' => 'icon-cogs'

            ),

            'input' => array(

                array(

                    'type' => 'text',

                    'label' => $this->l('Tokken de sécurité'),

                    'name' => 'CLIENT_TOKKEN',

                ),

            ),

            'submit' => array(

                'title' => $this->l('Save')

            )

        );

        $this->fields_form[2]['form'] = array(

            //ON RECUPERE LES PARAMETRES (CHOIX DE L'UTILISATEUR CONCERNANT SES PREFERENCES)

            'legend' => array(

                'title' => $this->l('Choix des paramètres'),

                'icon' => 'icon-cogs'

            ),

            'input' => array(

                array(

                    'type' => 'text',

                    'label' => $this->l('supplier_reference'),

                    'name' => 'supplier_reference',

                    'value' => 1

                ),

                array(

                    'type' => 'text',

                    'label' => $this->l('location'),

                    'name' => 'location',

                    'default' => 0

                ),

                array(

                    'type' => 'text',

                    'label' => $this->l('width'),

                    'name' => 'width',

                ),

                array(

                    'type' => 'text',

                    'label' => $this->l('height'),

                    'name' => 'height',

                ),

                array(

                    'type' => 'text',

                    'label' => $this->l('depth'),

                    'name' => 'depth',

                ),

                array(

                    'type' => 'text',

                    'label' => $this->l('weight'),

                    'name' => 'weight',

                ),

                array(

                    'type' => 'text',

                    'label' => $this->l('quantity_discount'),

                    'name' => 'quantity_discount',

                ),

                array(

                    'type' => 'text',

                    'label' => $this->l('ean13'),

                    'name' => 'ean13',

                ),

                array(

                    'type' => 'text',

                    'label' => $this->l('on_sale'),

                    'name' => 'on_sale',

                ),

                array(

                    'type' => 'text',

                    'label' => $this->l('online_only'),

                    'name' => 'online_only',

                ),

                array(

                    'type' => 'text',

                    'label' => $this->l('ecotax'),

                    'name' => 'ecotax',

                ),

                array(

                    'type' => 'text',

                    'label' => $this->l('minimal_quantity'),

                    'name' => 'minimal_quantity',

                ),

                array(

                    'type' => 'text',

                    'label' => $this->l('wholesale_price'),

                    'name' => 'wholesale_price',

                ),

                array(

                    'type' => 'text',

                    'label' => $this->l('customizable'),

                    'name' => 'customizable',

                ),

                array(

                    'type' => 'text',

                    'label' => $this->l('text_fields'),

                    'name' => 'text_fields',

                ),

                array(

                    'type' => 'text',

                    'label' => $this->l('uploadable_files'),

                    'name' => 'uploadable_files',

                ),

                array(

                    'type' => 'text',

                    'label' => $this->l('active'),

                    'name' => 'active',

                ),

                array(

                    'type' => 'text',

                    'label' => $this->l('available_for_order'),

                    'name' => 'available_for_order',

                ),

                array(

                    'type' => 'text',

                    'label' => $this->l('available_date'),

                    'name' => 'available_date',

                ),

                array(

                    'type' => 'text',

                    'label' => $this->l('show_price'),

                    'name' => 'show_price',

                ),

                array(

                    'type' => 'text',

                    'label' => $this->l('date_add'),

                    'name' => 'date_add',

                ),

                array(

                    'type' => 'text',

                    'label' => $this->l('date_upd'),

                    'name' => 'date_upd',

                ),

                array(

                    'type' => 'text',

                    'label' => $this->l('pack_stock_type'),

                    'name' => 'pack_stock_type',

                ),

                array(

                    'type' => 'text',

                    'label' => $this->l('meta_description'),

                    'name' => 'meta_description',

                ),

                array(

                    'type' => 'text',

                    'label' => $this->l('meta_keywords'),

                    'name' => 'meta_keywords',

                ),

                array(

                    'type' => 'text',

                    'label' => $this->l('meta_title'),

                    'name' => 'meta_title',

                ),

                array(

                    'type' => 'text',

                    'label' => $this->l('description'),

                    'name' => 'description',

                ),

                array(

                    'type' => 'text',

                    'label' => $this->l('description_short'),

                    'name' => 'description_short',

                ),

                array(

                    'type' => 'text',

                    'label' => $this->l('available_now'),

                    'name' => 'available_now',

                ),

                array(

                    'type' => 'text',

                    'label' => $this->l('available_later'),

                    'name' => 'available_later',

                ),

            ),

            'submit' => array(

                'title' => $this->l('Save')

                )

        );

        $this->fields_form[3]['form'] = array(

            //ON CREE UN BOUTON POUR LA SYNCRONISATION MANUEL QUI PERMET DE RECUPERER TOUT LES PRODUITS ET FAIRE DES UPDATES

            'legend' => array(

                'title' => $this->l('Synchronisation'),

                'icon' => 'icon-cogs'

            ), 

            'buttons' => array(

                'newBlock' => array(

                'title' => $this->l('Synchronisation'),

                //LIEN DU CONTROLLEUR FRONT DU MODULE CLIENT

                'href' => '/'.$Prestashop_dossier.'/index.php?fc=module&module=client&controller=synchro&tokken='.$tokken,

                'class' => 'pull-right',

                'icon' => 'process-icon-new'

                )

            )      

        );

        $helper = new HelperForm();

        $helper->show_toolbar = false;

        $helper->table = $this->table;

        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));

        $helper->default_form_language = $lang->id;

        $helper->module = $this;

        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;

        $helper->identifier = $this->identifier;



        $helper->submit_action = 'submitConfModule';



        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;

        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(

            'uri' => $this->getPathUri(),

            'fields_value' => $this->getConfigFieldsValues(),

            'languages' => $this->context->controller->getLanguages(),

            'id_language' => $this->context->language->id

        );

        return $helper->generateForm($this->fields_form);

    }



    //ON AFFICHE LES VALEURS POUR LA CONFIGURATION DU MODULE CLIENT

    public function getConfigFieldsValues(){ 

        $fields = array(

            'CLIENT_TOKKEN'=>Configuration::get('CLIENT_TOKKEN'),

            /*'reference'=>Configuration::get('reference'),*/

            'supplier_reference'=>Configuration::get('supplier_reference'),

            'location'=>Configuration::get('location'),

            'width'=>Configuration::get('width'),

            'height'=>Configuration::get('height'),

            'depth'=>Configuration::get('depth'),

            'weight'=>Configuration::get('weight'),

            'quantity_discount'=>Configuration::get('quantity_discount'),

            'ean13'=>Configuration::get('ean13'),

            'on_sale'=>Configuration::get('on_sale'),

            'online_only'=>Configuration::get('online_only'),

            'ecotax'=>Configuration::get('ecotax'),

            'minimal_quantity'=>Configuration::get('minimal_quantity'),

            'wholesale_price'=>Configuration::get('wholesale_price'),

            'customizable'=>Configuration::get('customizable'),

            'text_fields'=>Configuration::get('text_fields'),

            'uploadable_files'=>Configuration::get('uploadable_files'),

            'active'=>Configuration::get('active'),

            'available_for_order'=>Configuration::get('available_for_order'),

            'available_date'=>Configuration::get('available_date'),

            'show_price'=>Configuration::get('show_price'),

            'date_add'=>Configuration::get('date_add'),

            'date_upd'=>Configuration::get('date_upd'),

            'pack_stock_type'=>Configuration::get('pack_stock_type'),

            'meta_description'=>Configuration::get('meta_description'),

            'meta_keywords'=>Configuration::get('meta_keywords'),

            'meta_title'=>Configuration::get('meta_title'),

            'description'=>Configuration::get('description'),

            'description_short'=>Configuration::get('description_short'),

            'available_now'=>Configuration::get('available_now'),

            'available_later'=>Configuration::get('available_later'),

            'nom_prestashop'=>Configuration::get('nom_prestashop'),

            'adresse_centrale'=>Configuration::get('adresse_centrale'),

        );

        return $fields;

    }

}