<?php



    class ClientSynchroModuleFrontController extends ModuleFrontController{



    public function initContent(){

        parent::initContent();

        //SECURITE COTE CLIENT

        $tokken = Configuration::get('CLIENT_TOKKEN'); 

        if($_GET['tokken'] == $tokken){

            
            // lien pour tester le module du client

            //localhost/Client1/index.php?fc=module&module=client&controller=synchro



            //ON RECUPERE LES PREFERENCES DES CLIENTS POUR RECUPERER OU NON LES DONNEES DE LA CENTRALE

            //$reference = Configuration::get('reference'); OBLIGATOIRE POUR MODIFICATION

            $supplier_reference = Configuration::get('supplier_reference');

            $location = Configuration::get('location');

            $width = Configuration::get('width');   

            $height = Configuration::get('height');   

            $depth = Configuration::get('depth');   

            $weight = Configuration::get('weight');   

            $quantity_discount = Configuration::get('quantity_discount');

            $ean13 = Configuration::get('ean13');

            $on_sale = Configuration::get('on_sale');

            $online_only = Configuration::get('online_only');

            $ecotax = Configuration::get('ecotax');

            $minimal_quantity = Configuration::get('minimal_quantity');

            $wholesale_price = Configuration::get('wholesale_price');

            $customizable = Configuration::get('customizable');

            $text_fields = Configuration::get('text_fields');

            $uploadable_files = Configuration::get('uploadable_files');

            $active = Configuration::get('active');

            $available_for_order = Configuration::get('available_for_order');

            $available_date = Configuration::get('available_date');

            $show_price = Configuration::get('show_price');

            $date_add = Configuration::get('date_add');

            $date_upd = Configuration::get('date_upd');

            $pack_stock_type = Configuration::get('pack_stock_type');

            $meta_description = Configuration::get('meta_description');

            $meta_keywords = Configuration::get('meta_keywords');

            $meta_title = Configuration::get('meta_title');

            $description = Configuration::get('description');

            $description_short = Configuration::get('description_short');

            $available_now = Configuration::get('available_now');

            $available_later = Configuration::get('available_later');



            //LORS D'UN UPDATE RECUPERE l'id_produit et pour recuperer toutes les caractéristiques d'un seul produit en rapport avec son id

            if($_GET['update']){

                //INITIALISATION DE CURL VERS LE MODULE CENTRALE

                define('PRESTASHOP_URL',''.Configuration::get('adresse_centrale').'/index.php?fc=module&module=centrale&controller=synchro&tokken='.$tokken.'&update='.$_GET['update']);

                    

                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, PRESTASHOP_URL); 

                curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                $json = curl_exec($ch);

                $datas = json_decode($json);



                $product = $datas->results;



                $sql = 'SELECT id_product 

                        FROM '._DB_PREFIX_.'product

                        WHERE reference = "'.$product->reference.'"';

                $id_products = Db::getInstance()->executeS($sql);



                //INITIALISATION POUR l UPDATE AVEC LES INFORMATIONS RECUPERE de $product

                $new_product = new Product($id_products[0]['id_product'], 1);

                $new_product->price = $product->price; // OBLIGATOIRE

                $new_product->id_shop_default = $product->id_shop_default;

                $new_product->id_manufacturer = $product->id_manufacturer;

                $new_product->id_supplier = $product->id_supplier;

                $new_product->reference = $product->reference; //OBLIGATOIRE POUR MODIFICATION

                if($supplier_reference == 0){$new_product->supplier_reference = $product->supplier_reference;}

                if($location == 0){$new_product->location = $product->location;}

                if($width == 0){$new_product->width = $product->width;}

                if($height == 0){$new_product->height = $product->height;}

                if($depth == 0){$new_product->depth = $product->depth;}

                if($weight == 0){$new_product->weight = $product->weight;}

                if($quantity_discount == 0){$new_product->quantity_discount = $product->quantity_discount;}

                if($online_only == 0){$new_product->ean13 = $product->ean13;}

                $new_product->upc = $product->upc;

                $new_product->cache_is_pack = $product->cache_is_pack;

                $new_product->cache_has_attachments = $product->cache_has_attachments;

                $new_product->is_virtual = $product->is_virtual;



                $new_product->id_category_default = $product->id_category_default;

                $new_product->id_tax_rules_group = $product->id_tax_rules_group;

                if($on_sale == 0){$new_product->on_sale = $product->on_sale;}

                if($online_only == 0){$new_product->online_only = $product->online_only;}

                if($ecotax == 0){$new_product->ecotax = $product->ecotax;}

                if($minimal_quantity == 0){$new_product->minimal_quantity = $product->minimal_quantity;}

                if($wholesale_price == 0){$new_product->wholesale_price = $product->wholesale_price;}

                $new_product->unit_price_ratio = $product->unit_price_ratio;

                $new_product->additional_shipping_cost = $product->additional_shipping_cost;

                if($customizable == 0){$new_product->customizable = $product->customizable;}

                if($text_fields == 0){$new_product->text_fields = $product->text_fields;}

                if($uploadable_files == 0){$new_product->uploadable_files = $product->uploadable_files;}

                if($active == 0){$new_product->active = $product->active;}

                $new_product->redirect_type = $product->redirect_type;

                $new_product->id_product_redirected = $product->id_product_redirected;

                if($available_for_order == 0){$new_product->available_for_order = $product->available_for_order;}

                if($available_date == 0){$new_product->available_date = $product->available_date;}

                $new_product->condition = $product->condition;

                if($show_price == 0){$new_product->show_price = $product->show_price;}

                $new_product->indexed = $product->indexed;

                $new_product->visibility = $product->visibility;

                $new_product->cache_default_attribute = $product->cache_default_attribute;

                $new_product->advanced_stock_management = $product->advanced_stock_management;

                if($date_add == 0){$new_product->date_add = $product->date_add;}

                if($date_upd == 0){$new_product->date_upd = $product->date_upd;}

                if($pack_stock_type == 0){$new_product->pack_stock_type = $product->pack_stock_type;}



                $new_product->name = array(1 => $product->name); // OBLIGATOIRE 

                $new_product->link_rewrite = array(1 => $product->link_rewrite); //OBLIGATOIRE

                if($meta_description == 0){$new_product->meta_description = array(1 => $product->meta_description);}

                if($meta_keywords == 0){$new_product->meta_keywords = array(1 => $product->meta_keywords);}

                if($meta_title == 0){$new_product->meta_title = array(1 => $product->meta_title);}

                if($description == 0){$new_product->description = array(1 => $product->description);}

                if($description_short == 0){$new_product->description_short = array(1 => $product->description_short);}

                if($available_now == 0){$new_product->available_now = array(1 => $product->available_now);}

                if($available_later == 0){$new_product->available_later = array(1 => $product->available_later);}



                $new_product->save();



                die();

                curl_close($ch);



            //ON RECUPERE LA REFERENCE VENANT DE LA CENTRALE POUR RECUPERER LES INFORMATIONS DE CE PRODUIT EN LIAISON AVEC SA REFERENCE

            }elseif($_GET['reference']){

                //ON INITIALISE CURL VERS LA CENTRALE

                define('PRESTASHOP_URL',''.Configuration::get('adresse_centrale').'/index.php?fc=module&module=centrale&controller=synchro&tokken='.$tokken.'&reference='.$_GET['reference']);

                    

                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, PRESTASHOP_URL); 

                curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                $json = curl_exec($ch);

                $datas = json_decode($json);



                $product = $datas->results;



                //INITIALISATION D'UN AJOUT DE PRODUIT

                $new_product = new Product(null, 1);

                $new_product->price = $product->price; // OBLIGATOIRE

                $new_product->id_shop_default = $product->id_shop_default;

                $new_product->id_manufacturer = $product->id_manufacturer;

                $new_product->id_supplier = $product->id_supplier;

                $new_product->reference = $product->reference; //OBLIGATOIRE POUR MODIFICATION

                if($supplier_reference == 0){$new_product->supplier_reference = $product->supplier_reference;}

                if($location == 0){$new_product->location = $product->location;}

                if($width == 0){$new_product->width = $product->width;}

                if($height == 0){$new_product->height = $product->height;}

                if($depth == 0){$new_product->depth = $product->depth;}

                if($weight == 0){$new_product->weight = $product->weight;}

                if($quantity_discount == 0){$new_product->quantity_discount = $product->quantity_discount;}

                if($online_only == 0){$new_product->ean13 = $product->ean13;}

                $new_product->upc = $product->upc;

                $new_product->cache_is_pack = $product->cache_is_pack;

                $new_product->cache_has_attachments = $product->cache_has_attachments;

                $new_product->is_virtual = $product->is_virtual;



                $new_product->id_category_default = $product->id_category_default;

                $new_product->id_tax_rules_group = $product->id_tax_rules_group;

                if($on_sale == 0){$new_product->on_sale = $product->on_sale;}

                if($online_only == 0){$new_product->online_only = $product->online_only;}

                if($ecotax == 0){$new_product->ecotax = $product->ecotax;}

                if($minimal_quantity == 0){$new_product->minimal_quantity = $product->minimal_quantity;}

                if($wholesale_price == 0){$new_product->wholesale_price = $product->wholesale_price;}

                $new_product->unit_price_ratio = $product->unit_price_ratio;

                $new_product->additional_shipping_cost = $product->additional_shipping_cost;

                if($customizable == 0){$new_product->customizable = $product->customizable;}

                if($text_fields == 0){$new_product->text_fields = $product->text_fields;}

                if($uploadable_files == 0){$new_product->uploadable_files = $product->uploadable_files;}

                if($active == 0){$new_product->active = $product->active;}

                $new_product->redirect_type = $product->redirect_type;

                $new_product->id_product_redirected = $product->id_product_redirected;

                if($available_for_order == 0){$new_product->available_for_order = $product->available_for_order;}

                if($available_date == 0){$new_product->available_date = $product->available_date;}

                $new_product->condition = $product->condition;

                if($show_price == 0){$new_product->show_price = $product->show_price;}

                $new_product->indexed = $product->indexed;

                $new_product->visibility = $product->visibility;

                $new_product->cache_default_attribute = $product->cache_default_attribute;

                $new_product->advanced_stock_management = $product->advanced_stock_management;

                if($date_add == 0){$new_product->date_add = $product->date_add;}

                if($date_upd == 0){$new_product->date_upd = $product->date_upd;}

                if($pack_stock_type == 0){$new_product->pack_stock_type = $product->pack_stock_type;}



                $new_product->name = array(1 => $product->name); // OBLIGATOIRE 

                $new_product->link_rewrite = array(1 => $product->link_rewrite); //OBLIGATOIRE

                if($meta_description == 0){$new_product->meta_description = array(1 => $product->meta_description);}

                if($meta_keywords == 0){$new_product->meta_keywords = array(1 => $product->meta_keywords);}

                if($meta_title == 0){$new_product->meta_title = array(1 => $product->meta_title);}

                if($description == 0){$new_product->description = array(1 => $product->description);}

                if($description_short == 0){$new_product->description_short = array(1 => $product->description_short);}

                if($available_now == 0){$new_product->available_now = array(1 => $product->available_now);}

                if($available_later == 0){$new_product->available_later = array(1 => $product->available_later);}



                $new_product->save();



                die();

                curl_close($ch);



            //SYNCRONISATION MANUEL DU CLIENT POUR RECUPERER L'ENSEMBLE DE LA BASE DE DONNEE DE LA CENTRALE POUR LES PRODUITS

            }else{

                // Curl pour récupérer les données

                //Define pour le prestashop de la centrale 

                define('PRESTASHOP_URL',''.Configuration::get('adresse_centrale').'/index.php?fc=module&module=centrale&controller=synchro&tokken='.$tokken);

                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, PRESTASHOP_URL); 

                curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                $json = curl_exec($ch);

                $datas = json_decode($json);



                //ON BOUCLE SUR CHAQUE PRODUIT

                foreach($datas->results as $product){

                    $sql = 'SELECT id_product 

                            FROM '._DB_PREFIX_.'product

                            WHERE reference = "'.$product->reference.'"';

                    $id_products = Db::getInstance()->executeS($sql);

                            

                    //ON TESTE SI CA EXISTE DEJA ET DANS CE CAS ON UPDATE

                    if(isset($id_products[0]['id_product']) && $id_products[0]['id_product']){

                        $new_product = new Product($id_products[0]['id_product'], 1);

                        //SINON ON CREE LE NOUVEAU PRODUIT

                    }else{

                        $new_product = new Product(null, 1);

                    }

                    $new_product->price = $product->price; // OBLIGATOIRE

                    $new_product->id_shop_default = $product->id_shop_default;

                    $new_product->id_manufacturer = $product->id_manufacturer;

                    $new_product->id_supplier = $product->id_supplier;

                    $new_product->reference = $product->reference; //OBLIGATOIRE POUR MODIFICATION

                    if($supplier_reference == 0){$new_product->supplier_reference = $product->supplier_reference;}

                    if($location == 0){$new_product->location = $product->location;}

                    if($width == 0){$new_product->width = $product->width;}

                    if($height == 0){$new_product->height = $product->height;}

                    if($depth == 0){$new_product->depth = $product->depth;}

                    if($weight == 0){$new_product->weight = $product->weight;}

                    if($quantity_discount == 0){$new_product->quantity_discount = $product->quantity_discount;}

                    if($online_only == 0){$new_product->ean13 = $product->ean13;}

                    $new_product->upc = $product->upc;

                    $new_product->cache_is_pack = $product->cache_is_pack;

                    $new_product->cache_has_attachments = $product->cache_has_attachments;

                    $new_product->is_virtual = $product->is_virtual;



                    $new_product->id_category_default = $product->id_category_default;

                    $new_product->id_tax_rules_group = $product->id_tax_rules_group;

                    if($on_sale == 0){$new_product->on_sale = $product->on_sale;}

                    if($online_only == 0){$new_product->online_only = $product->online_only;}

                    if($ecotax == 0){$new_product->ecotax = $product->ecotax;}

                    if($minimal_quantity == 0){$new_product->minimal_quantity = $product->minimal_quantity;}

                    if($wholesale_price == 0){$new_product->wholesale_price = $product->wholesale_price;}

                    $new_product->unit_price_ratio = $product->unit_price_ratio;

                    $new_product->additional_shipping_cost = $product->additional_shipping_cost;

                    if($customizable == 0){$new_product->customizable = $product->customizable;}

                    if($text_fields == 0){$new_product->text_fields = $product->text_fields;}

                    if($uploadable_files == 0){$new_product->uploadable_files = $product->uploadable_files;}

                    if($active == 0){$new_product->active = $product->active;}

                    $new_product->redirect_type = $product->redirect_type;

                    $new_product->id_product_redirected = $product->id_product_redirected;

                    if($available_for_order == 0){$new_product->available_for_order = $product->available_for_order;}

                    if($available_date == 0){$new_product->available_date = $product->available_date;}

                    $new_product->condition = $product->condition;

                    if($show_price == 0){$new_product->show_price = $product->show_price;}

                    $new_product->indexed = $product->indexed;

                    $new_product->visibility = $product->visibility;

                    $new_product->cache_default_attribute = $product->cache_default_attribute;

                    $new_product->advanced_stock_management = $product->advanced_stock_management;

                    if($date_add == 0){$new_product->date_add = $product->date_add;}

                    if($date_upd == 0){$new_product->date_upd = $product->date_upd;}

                    if($pack_stock_type == 0){$new_product->pack_stock_type = $product->pack_stock_type;}



                    $new_product->name = array(1 => $product->name); // OBLIGATOIRE 

                    $new_product->link_rewrite = array(1 => $product->link_rewrite); //OBLIGATOIRE

                    if($meta_description == 0){$new_product->meta_description = array(1 => $product->meta_description);}

                    if($meta_keywords == 0){$new_product->meta_keywords = array(1 => $product->meta_keywords);}

                    if($meta_title == 0){$new_product->meta_title = array(1 => $product->meta_title);}

                    if($description == 0){$new_product->description = array(1 => $product->description);}

                    if($description_short == 0){$new_product->description_short = array(1 => $product->description_short);}

                    if($available_now == 0){$new_product->available_now = array(1 => $product->available_now);}

                    if($available_later == 0){$new_product->available_later = array(1 => $product->available_later);}



                    $new_product->save();

                }

            die();

            curl_close($ch);

            }

        }

    }  

}